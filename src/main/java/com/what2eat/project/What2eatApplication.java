package com.what2eat.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class What2eatApplication {

    public static void main(String[] args) {
        SpringApplication.run(What2eatApplication.class, args);
    }
}
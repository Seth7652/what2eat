package com.what2eat.project.config;

import com.what2eat.project.converters.MyGsonHttpMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import java.util.List;
/**
 * Created by Aleksandar on 1/14/2017.
 */
@Configuration
public class AppWebMvcConfiguration extends WebMvcConfigurerAdapter {

    private MyGsonHttpMessageConverter g;

    @Autowired
    public void setMyGsonHttpMessageConverter(MyGsonHttpMessageConverter g){
        this.g = g;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter< ? >> converters) {
        converters.add(g);
    }
}
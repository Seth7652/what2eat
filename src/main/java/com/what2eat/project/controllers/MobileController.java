package com.what2eat.project.controllers;

import com.what2eat.project.models.Recipe;
import com.what2eat.project.models.RecipeItem;
import com.what2eat.project.models.SearchResult;
import com.what2eat.project.services.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Aleksandar on 1/6/2017.
 */
@RestController
public class MobileController {

    private RequestService requestService;

    @Autowired
    public void setRequestService(RequestService requestService){
        this.requestService = requestService;
    }

    @RequestMapping(value = "/recipe/{id}",  method = RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
    public Recipe getRecepie(@PathVariable("id") String id){
        RecipeItem r = requestService.getRecipe(id);
        return r.getRecipe();
    }

    @RequestMapping(value = "/results/{query}",  method = RequestMethod.GET, produces={MediaType.APPLICATION_JSON_VALUE})
    public List<Recipe> getResults(@PathVariable("query") String query){
        SearchResult s = requestService.listRecepies();
        return s.getRecipes();
    }

    @RequestMapping(value = "/register",  method = RequestMethod.GET)
    public void register(){
        requestService.addUser("test");
    }

    @RequestMapping(value = "/add_favorite/{id}",  method = RequestMethod.GET)
    public boolean addToFavorites(@PathVariable String id){
        RecipeItem r = requestService.getRecipe(id);
        Recipe recipe = r.getRecipe();

        return false;
    }

    public List<Recipe> getFavorites(Authentication authentication){

        return null;
    }

}



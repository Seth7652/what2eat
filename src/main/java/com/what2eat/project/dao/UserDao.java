package com.what2eat.project.dao;

import javax.transaction.Transactional;
import com.what2eat.project.models.User;
import org.springframework.data.repository.CrudRepository;



@Transactional
public interface UserDao extends CrudRepository<User, String> {

}
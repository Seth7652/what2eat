package com.what2eat.project.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Aleksandar on 1/6/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecipeItem {

    public Recipe recipe;

    public RecipeItem() {
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public String toString(){
        return this.recipe.toString();
    }
}

package com.what2eat.project.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

/**
 * Created by Aleksandar on 1/14/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResult {

    private int count;
    private List<Recipe> recipes;

    public SearchResult() {
    }

    public int getCount() {
        return count;
    }

    public List<Recipe> getRecipes() {
        return recipes;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes = recipes;
    }

    @Override
    public String toString(){
        return String.format("%s", recipes.size());
    }
}
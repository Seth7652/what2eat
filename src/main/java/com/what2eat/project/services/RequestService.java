package com.what2eat.project.services;

import com.what2eat.project.converters.MyGsonHttpMessageConverter;
import com.what2eat.project.dao.UserDao;
import com.what2eat.project.models.Recipe;
import com.what2eat.project.models.RecipeItem;
import com.what2eat.project.models.SearchResult;
import com.what2eat.project.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by Aleksandar on 1/14/2017.
 */
@Service
public class RequestService {

    @Autowired
    private UserDao userDao;

    public List<Recipe> getFavorites(String token){
       User u =  userDao.findOne(token);
       return u.getFavorites();
    }

    public void addUser(String token){
        User u = new User();
        u.setToken(token);
        userDao.save(u);
    }

    public RecipeItem getRecipe(String id){
        String url = "https://community-food2fork.p.mashape.com/get?key=fd381641dd0a9e1612d92cf5fb1eda00&rId=";//29465";
        url = url + id;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MyGsonHttpMessageConverter());
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("X-Mashape-Key", "a7suz1Y3pzmsh7qktIPo0mdTmpkup1z0zXUjsnUuWvR2yClfMy");
        requestHeaders.add("Accept", "application/json");

        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
        ResponseEntity<RecipeItem> responseEntity  = restTemplate.exchange(url, HttpMethod.GET, requestEntity, RecipeItem.class);


        return responseEntity.getBody();
    }

    public SearchResult listRecepies(){
        String url = "http://food2fork.com/api/search?key=fd381641dd0a9e1612d92cf5fb1eda00&q=shredded%2C+chicken%2C+wings";
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MyGsonHttpMessageConverter());
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.add("X-Mashape-Key", "a7suz1Y3pzmsh7qktIPo0mdTmpkup1z0zXUjsnUuWvR2yClfMy");
        requestHeaders.add("Accept", "application/json");

        HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
        ResponseEntity<SearchResult> responseEntity  = restTemplate.exchange(url, HttpMethod.GET, requestEntity, SearchResult.class);

        return responseEntity.getBody();
    }
}
